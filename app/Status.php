<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'statuss';
    protected $fillable = ['status'];
    protected $primaryKey = 'status_id';
    public $timestamps=false;
}
