<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KaryawanController extends Controller
{
    public function __construct()
    {
      $this->middleware('web');
    }

    public function karyawan()
    {
      return view('layouts.karyawan.karyawan');
    }
}
