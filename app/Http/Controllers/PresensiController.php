<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Datatables;
use Excel;

use App\Presensi;

class PresensiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.dashboard.dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $data = [
          'name' => $request['name'],
          'tgl1' => $request['tgl1'],
          'tgl2' => $request['tgl2'],
          'tgl3' => $request['tgl3'],
          'tgl4' => $request['tgl4'],
          'tgl5' => $request['tgl5'],
          'tgl6' => $request['tgl6'],
          'tgl7' => $request['tgl7'],
          'tgl8' => $request['tgl8'],
          'tgl9' => $request['tgl9'],
          'tgl10' => $request['tgl10'],
          'tgl11' => $request['tgl11'],
          'tgl12' => $request['tgl12'],
          'tgl13' => $request['tgl13'],
          'tgl14' => $request['tgl14'],
          'tgl15' => $request['tgl15'],
          'tgl16' => $request['tgl16'],
          'tgl17' => $request['tgl17'],
          'tgl18' => $request['tgl18'],
          'tgl19' => $request['tgl19'],
          'tgl20' => $request['tgl20'],
          'tgl21' => $request['tgl21'],
          'tgl22' => $request['tgl22'],
          'tgl23' => $request['tgl23'],
          'tgl24' => $request['tgl24'],
          'tgl25' => $request['tgl25'],
          'tgl26' => $request['tgl26'],
          'tgl27' => $request['tgl27'],
          'tgl28' => $request['tgl28'],
          'tgl29' => $request['tgl29'],
          'tgl30' => $request['tgl30'],
          'tgl31' => $request['tgl31'],
          'hadir' => $request['hadir'],
          'izin' => $request['izin'],
          'alfa' => $request['alfa']
        ];

        return Presensi::create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $presensi = Presensi::find($id);
        return $presensi;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $presensi = Presensi::find($id);
         $presensi->name = $request['name'];
         $presensi->tgl1 = $request['tgl1'];
         $presensi->tgl2 = $request['tgl2'];
         $presensi->tgl3 = $request['tgl3'];
         $presensi->tgl4 = $request['tgl4'];
         $presensi->tgl5 = $request['tgl5'];
         $presensi->tgl6 = $request['tgl6'];
         $presensi->tgl7 = $request['tgl7'];
         $presensi->tgl8 = $request['tgl8'];
         $presensi->tgl9 = $request['tgl9'];
         $presensi->tgl10 = $request['tgl10'];
         $presensi->tgl11 = $request['tgl11'];
         $presensi->tgl12 = $request['tgl12'];
         $presensi->tgl13 = $request['tgl13'];
         $presensi->tgl14 = $request['tgl14'];
         $presensi->tgl15 = $request['tgl15'];
         $presensi->tgl16 = $request['tgl16'];
         $presensi->tgl17 = $request['tgl17'];
         $presensi->tgl18 = $request['tgl18'];
         $presensi->tgl19 = $request['tgl19'];
         $presensi->tgl20 = $request['tgl20'];
         $presensi->tgl21 = $request['tgl21'];
         $presensi->tgl22 = $request['tgl22'];
         $presensi->tgl23 = $request['tgl23'];
         $presensi->tgl24 = $request['tgl24'];
         $presensi->tgl25 = $request['tgl25'];
         $presensi->tgl26 = $request['tgl16'];
         $presensi->tgl27 = $request['tgl27'];
         $presensi->tgl28 = $request['tgl28'];
         $presensi->tgl29 = $request['tgl29'];
         $presensi->tgl30 = $request['tgl30'];
         $presensi->tgl31 = $request['tgl31'];
         $presensi->hadir = $request['hadir'];
         $presensi->izin = $request['izin'];
         $presensi->alfa = $request['alfa'];
         $presensi->update();

         return $presensi;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Presensi::destroy($id);
    }

    public function apiPresensi()
    {
      $presensi = Presensi::all();

      return Datatables::of($presensi)
          ->addColumn('action', function($presensi){
              return
                     '<a onclick="editForm('. $presensi->id .')" class="btn btn-primary btn-xs"></i><i class="fa fa-pencil-square-o"></i> Edit</a> ' .
                     '<a onclick="deleteData('. $presensi->id .')" class="btn btn-danger btn-xs"></i><i class="fa fa-trash-o"></i> Delete</a>';
          })->make(true);
    }

    public function presensiExport(){
      $presensi = Presensi::select('id','name','tgl1','tgl2','tgl3','tgl4','tgl5','tgl6','tgl7','tgl8','tgl9','tgl10','tgl11','tgl12','tgl13','tgl14','tgl15','tgl16','tgl17','tgl18','tgl19','tgl20',
      'tgl21','tgl22','tgl23','tgl24','tgl25','tgl26','tgl27','tgl28','tgl29','tgl30','tgl31','hadir','izin','alfa')->get();
      return Excel::create('2017 Desember', function($excel) use ($presensi){
            $excel->sheet('mysheet', function($sheet) use ($presensi){
            $sheet->fromArray($presensi);
        });
      })->download('xlsx');
    }

    public function presensiImport(Request $request)
    {
      if ($request->hasFile('file'))
      {
        $path = $request->file('file')->getRealPath();
        $data = Excel::load($path, function($reader){})->get();
        if (!empty($data) && $data->count())
        {
          foreach ($data as $key => $value)
          {
            $presensi = new Presensi();
            $presensi->name = $value->name;
            $presensi->tgl1 = $value->tgl1;
            $presensi->tgl2 = $value->tgl2;
            $presensi->tgl3 = $value->tgl3;
            $presensi->tgl4 = $value->tgl4;
            $presensi->tgl5 = $value->tgl5;
            $presensi->tgl6 = $value->tgl6;
            $presensi->tgl7 = $value->tgl7;
            $presensi->tgl8 = $value->tgl8;
            $presensi->tgl9 = $value->tgl9;
            $presensi->tgl10 = $value->tgl10;
            $presensi->tgl11 = $value->tgl11;
            $presensi->tgl12 = $value->tgl12;
            $presensi->tgl13 = $value->tgl13;
            $presensi->tgl14 = $value->tgl14;
            $presensi->tgl15 = $value->tgl15;
            $presensi->tgl16 = $value->tgl16;
            $presensi->tgl17 = $value->tgl17;
            $presensi->tgl18 = $value->tgl18;
            $presensi->tgl19 = $value->tgl19;
            $presensi->tgl20 = $value->tgl20;
            $presensi->tgl21 = $value->tgl21;
            $presensi->tgl22 = $value->tgl22;
            $presensi->tgl23 = $value->tgl23;
            $presensi->tgl24 = $value->tgl24;
            $presensi->tgl25 = $value->tgl25;
            $presensi->tgl26 = $value->tgl26;
            $presensi->tgl27 = $value->tgl27;
            $presensi->tgl28 = $value->tgl28;
            $presensi->tgl29 = $value->tgl29;
            $presensi->tgl30 = $value->tgl30;
            $presensi->tgl31 = $value->tgl31;
            $presensi->hadir = $value->hadir;
            $presensi->izin = $value->izin;
            $presensi->alfa = $value->alfa;
            $presensi->save();
          }
        }
      }
      return back();
    }
}
