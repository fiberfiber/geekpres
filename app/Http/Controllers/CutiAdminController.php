<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tahun;
use App\NamaCuti;
use App\Developer;
use App\Alasan;
use App\Kerja;
use App\Time;
use App\Jumlah;
use App\Status;
use App\DataCuti;

class CutiAdminController extends Controller
{
  public function __construct()
  {
    $this->middleware('web');
  }

  public function cutiadmin()
  {
    $names = NamaCuti::all();
    $kerjas = Kerja::all();
    $times = Time::all();
    $jumlahs = Jumlah::all();
    $alasans = Alasan::all();
    $statuss = Status::all();
    $tahuns = Tahun::orderBy('tahun_id','DECS')->get();
    return view('layouts.laporan.cutiadmin',compact('names','tahuns','alasans','kerjas','times','jumlahs','statuss'));
  }

  public function postInsertTahun(Request $request)
  {
    if ($request->ajax())
    {
      return response(Tahun::create($request->all()));
    }
  }

  public function postInsertName(Request $request)
  {
    if ($request->ajax())
    {
      return response(NamaCuti::create($request->all()));
    }
  }

  public function postInsertDeveloper(Request $request)
  {
    if ($request->ajax())
    {
      return response(Developer::create($request->all()));
    }
  }

  public function showDeveloper(Request $request)
  {
    if ($request->ajax())
    {
      return response(Developer::where('name_id',$request->name_id)->get());
    }
  }

  public function createAlasan(Request $request)
  {
    if ($request->ajax())
    {
      return (Alasan::create($request->all()));
    }
  }

  public function createKerja(Request $request)
  {
    if ($request->ajax())
    {
      return (Kerja::create($request->all()));
    }
  }

  public function createTime(Request $request)
  {
    if ($request->ajax())
    {
      return (Time::create($request->all()));
    }
  }

  public function createJumlah(Request $request)
  {
    if ($request->ajax())
    {
      return (Jumlah::create($request->all()));
    }
  }

  public function createStatus(Request $request)
  {
    if ($request->ajax())
    {
      return (Status::create($request->all()));
    }
  }

  public function createData(Request $request)
  {
    if ($request->ajax())
    {
      return response(DataCuti::create($request->all()));
    }
  }

  public function showDataCuti(Request $request)
  {
      $datacutis = $this->sshowDataCuti()->get();
      return view('data.infodatacuti',compact('datacutis'));
  }

  public function sshowDataCuti()
  {
    return DataCuti::join('tahuns','tahuns.tahun_id','=','datacutis.tahun_id')
                           ->join('developers','developers.developer_id','=','datacutis.developer_id')
                           ->join('names','names.name_id','=','developers.name_id')
                           ->join('alasans','alasans.alasan_id','=','datacutis.alasan_id')
                           ->join('kerjas','kerjas.kerja_id','=','datacutis.kerja_id')
                           ->join('times','times.time_id','=','datacutis.time_id')
                           ->join('jumlahs','jumlahs.jumlah_id','=','datacutis.jumlah_id')
                           ->join('statuss','statuss.status_id','=','datacutis.status_id')
                           ->orderBy('datacutis.datacuti_id','DESC');
  }

  public function deleteData(Request $request)
  {
      if ($request->ajax())
      {
          DataCuti::destroy($request->datacuti_id);
      }
  }

  public function editData(Request $request)
  {
    if ($request->ajax())
    {
        return response(DataCuti::find($request->datacuti_id));
    }
  }

  public function updateData(Request $request)
  {
    if ($request->ajax())
    {
        return response(DataCuti::updateOrCreate(['datacuti_id'=>$request->datacuti_id],$request->all()));
    }
  }

}
