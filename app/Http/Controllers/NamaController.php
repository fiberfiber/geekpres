<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nama;

class NamaController extends Controller
{
  public function __construct()
  {
    $this->middleware('web');
  }

  public function getNama()
  {
    return view('layouts.karyawan.nama');
  }
}
