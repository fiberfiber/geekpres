<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Charts;

use App\Presensi;

class DashboardController extends Controller
{
    public function __construct()
    {
      $this->middleware('web');
    }

    public function dashboard()
    {
      $grafik = Presensi::where('tgl1','1')->get();
      $chart = Charts::create('bar', 'c3')
          ->title('Jumlah Kehadiran')
          ->elementLabel("Desember 2017")
          ->responsive(false)
          ->labels($grafik->pluck('name'))
          ->values($grafik->pluck('hadir'));
      return view('layouts.dashboard.dashboard', ['chart' => $chart]);
    }
}
