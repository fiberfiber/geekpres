<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataCuti extends Model
{
    protected $table = 'datacutis';
    protected $fillable = ['tahun_id','developer_id','alasan_id','kerja_id','time_id','jumlah_id','status_id',
                          'start_date','end_date','active'];
    protected $primaryKey = 'datacuti_id';
    public $timestamps=false;
}
