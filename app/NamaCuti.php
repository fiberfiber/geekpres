<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NamaCuti extends Model
{
    protected $table = 'names';
    protected $fillable = ['name'];
    protected $primaryKey = 'name_id';
    public $timestamps=false;
}
