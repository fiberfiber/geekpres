<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jumlah extends Model
{
    protected $table = 'jumlahs';
    protected $fillable = ['jumlah'];
    protected $primaryKey = 'jumlah_id';
    public $timestamps=false;
}
