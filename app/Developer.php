<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Developer extends Model
{
    protected $table = 'developers';
    protected $fillable = ['name_id','developer'];
    protected $primaryKey = 'developer_id';
    public $timestamps=false;
}
