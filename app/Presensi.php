<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presensi extends Model
{
    protected $fillable = ['name','tgl1', 'tgl2', 'tgl3', 'tgl4', 'tgl5', 'tgl6', 'tgl7', 'tgl8', 'tgl9', 'tgl10', 'tgl11', 'tgl12', 'tgl13', 'tgl14', 'tgl15', 'tgl16', 'tgl17',
    'tgl18', 'tgl19', 'tgl20', 'tgl21', 'tgl22', 'tgl23', 'tgl24', 'tgl25', 'tgl26', 'tgl27', 'tgl28', 'tgl29','tgl30','tgl31','hadir','izin','alfa'];
}
