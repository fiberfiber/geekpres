<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alasan extends Model
{
    protected $table = 'alasans';
    protected $fillable = ['alasan'];
    protected $primaryKey = 'alasan_id';
    public $timestamps=false;
}
