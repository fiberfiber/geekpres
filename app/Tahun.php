<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tahun extends Model
{
    protected $table = 'tahuns';
    protected $fillable = ['tahun'];
    protected $primaryKey = 'tahun_id';
    public $timestamps=false;
}
