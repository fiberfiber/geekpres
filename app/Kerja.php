<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kerja extends Model
{
    protected $table = 'kerjas';
    protected $fillable = ['kerja'];
    protected $primaryKey = 'kerja_id';
    public $timestamps=false;
}
