@extends('layouts.master')
@section('content')

<div class="container">
  <h4 class="text-center">
    <div class="o-layout__item u-4of12">
      <h1 class="u-fg--ash-light u-txt--bold" style="font-size: 80px; margin-top: 110px;">404</h1>
      <p class="u-txt--bold u-txt--xlarge">Maaf, halaman yang kamu tuju tidak ditemukan.</p>
      <p>
        Halaman ini adalah halaman khusus admin atau kembali ke
        <a title="Halaman Depan" href="{{route('dashboard')}}">halaman depan</a>.
      </p>
    </div>
    <div class="o-layout__item u-8of12 u-align-center">
      <img title="Halaman Tidak Ditemukan" width="640" height="450" src="https://s2.bukalapak.com/images/404.png" alt="404">
    </div>
  </h4>
</div>


@endsection
