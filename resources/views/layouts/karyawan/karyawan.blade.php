<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <link rel="shortcut icon" href="img/geekgarden.png">

    <title>{{ Auth::user()->name}} Geekpres | Aplikasi Manajemen Kehadiran Karyawan</title>

    <link href="{{ asset('assets/bootstrap/css/scrollmenu.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- <link href="{{ asset('assets/datatables/css/dataTables.bootstrap.min.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('assets/bootstrap/css/ie10-viewport-bug-workaround.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/bootstrap/css/navbar-fixed-top.css') }}" rel="stylesheet">
    <script src="{{ asset('assets/bootstrap/js/ie-emulation-modes-warning.js') }}"></script>

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <script src="{{ asset('assets/sweetalert2/sweetalert2.min.js') }}"></script>
    <link href="{{ asset('assets/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="css/elegant-icons-style.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <!-- full calendar css-->
    <link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
    <link href="assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen" />
    <!-- owl carousel -->
    <link rel="stylesheet" href="css/owl.carousel.css" type="text/css">
    <link href="css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
    <link rel="stylesheet" href="css/fullcalendar.css">
    <link href="css/widgets.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
    <link href="css/xcharts.min.css" rel=" stylesheet">
    <link href="css/jquery-ui-1.10.4.min.css" rel="stylesheet">
  </head>

  <body>

{{------------------------------------------------------------------------------------------------------------------------------------------------------------------}}
<header class="header dark-bg">
  <div class="toggle-nav">
    <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
  </div>
  <a href="{{route('dashboard')}}" class="logo">Geek <span class="lite">Pres</span></a>

  <div class="top-nav notification-row">
    <ul class="nav pull-right top-menu">
      <li class="dropdown">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="username">{{ Auth::user()->name}}</span>
                        <b class="caret"></b>
                    </a>
        <ul class="dropdown-menu extended">
          <div class="log-arrow-up"></div>
          <li>
            <a href="{{route('logout')}}"><i class="fa fa-sign-out"></i> Log Out</a>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</header>

{{-----------------------------------------------------------}}
<aside>
  <div id="sidebar" class="nav-collapse ">
    <ul class="sidebar-menu">
      <li>
        <a class="" href="{{route('dashboard')}}">
                      <i class="icon_house_alt"></i>
                      <span>Dashboard</span>
                  </a>
      </li>
      <li class="sub-menu">
        <a href="javascript:;" class="">
                      <i class="fa fa-user"></i>
                      <span>Karyawan</span>
                      <span class="menu-arrow arrow_carrot-right"></span>
                  </a>
        <ul class="sub">
          <li><a class="" href="{{route('nama')}}">Nama Karyawan</a></li>
          <li><a class="" href="{{route('karyawan')}}">Data Admin</a></li>
        </ul>
      </li>

      <li class="sub-menu">
        <a href="javascript:;" class="">
                      <i class="fa fa-bullhorn"></i>
                      <span>Laporan</span>
                      <span class="menu-arrow arrow_carrot-right"></span>
                  </a>
        <ul class="sub">
          <li><a href="{{route('cuti')}}">Cuti</a></li>
          <li><a href="{{route('cutiadmin')}}">Cuti Admin</a></li>
        </ul>
      </li>

    </ul>
  </div>
</aside>
{{----------------------------------------------------------------------}}

    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-md-12">
            <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header">
        <i class="fa fa-user"></i>
        Karyawan
      </h3>
      <ol class="breadcrumb">
        <li>
          <i class="fa fa-home">
            <a href="{{route('dashboard')}}" style="color: #797979">Dashboard</a>
           </i>
        </li>

        <li>
          <i class="fa fa-user">
            <a href="{{route('nama')}}" style="color: #797979">Nama Karyawan</a>
           </i>
        </li>

        <li>
          <i class="fa fa-file-text">
            <a href="{{route('karyawan')}}" style="color: #797979">Data Admin</a>
           </i>
        </li>
      </ol>
    </div>
  </div>

  <div class="row">
    <div class="col-md-3">
      <div class="panel panel-default">
        <header class="panel-heading text-center">Hadir Terbanyak</header>
        <div class="panel-body blue-bg">
          <div class="text-center">
            Ade Indra Putra
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-3">
      <div class="panel panel-default">
        <header class="panel-heading text-center">Izin Terbanyak</header>
        <div class="panel-body brown-bg">
          <div class="text-center">
            Nassharih Abdulloh
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-3">
      <div class="panel panel-default">
        <header class="panel-heading text-center">Alfa Terbanyak</header>
        <div class="panel-body dark-bg">
          <div class="text-center">
            Irfan Zainul Abidin
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-3">
      <div class="panel panel-default">
        <header class="panel-heading text-center">Hadiah Bulan Ini</header>
        <div class="panel-body green-bg">
          <div class="text-center">
            IPhone-X
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
          <header class="panel-heading">Data Kehadiran
              <a onclick="addForm()" class="pull-right" id="show-class-info"><i class="fa fa-plus"></i></a>
              <a onclick="eximForm()" class="pull-right" id="show-class-info"><i class="fa fa-cloud-upload"></i></a>
          </header>
      <div class="panel-body">
        <div class="col-md-12">
            <table id="presensi-table" class="table table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>1</th>
                        <th>2</th>
                        <th>3</th>
                        <th>4</th>
                        <th>5</th>
                        <th>6</th>
                        <th>7</th>
                        <th>8</th>
                        <th>9</th>
                        <th>10</th>
                        <th>11</th>
                        <th>12</th>
                        <th>13</th>
                        <th>14</th>
                        <th>15</th>
                        <th>16</th>
                        <th>17</th>
                        <th>18</th>
                        <th>20</th>
                        <th>21</th>
                        <th>22</th>
                        <th>23</th>
                        <th>24</th>
                        <th>25</th>
                        <th>26</th>
                        <th>27</th>
                        <th>28</th>
                        <th>29</th>
                        <th>30</th>
                        <th>31</th>
                        <th class="text-center blue-bg">Hadir</th>
                        <th class="text-center brown-bg">Izin</th>
                        <th class="text-center dark-bg">Alfa</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('layouts.dashboard.tambah')




</section>
</section>

  <script src="{{ asset('assets/jquery/jquery-1.12.4.min.js') }}"></script>
  <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>

  <script src="{{ asset('assets/dataTables/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/dataTables/js/dataTables.bootstrap.min.js') }}"></script>

  <script src="{{ asset('assets/validator/validator.min.js') }}"></script>
  <script src="{{ asset('assets/bootstrap/js/ie10-viewport-bug-workaround.js') }}"></script>
  <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="js/calendar-custom.js"></script>
  <script src="js/jquery.customSelect.min.js"></script>
  <script src="js/scripts.js"></script>

  <script type="text/javascript">
    var table = $('#presensi-table').DataTable({
    serverSide: true,
    ajax: "{{route('api.presensi')}}",
    columns: [
      {data: 'id', name: 'id'},
      {data: 'name', name: 'name'},
      {data: 'tgl1', name: 'tgl1'},
      {data: 'tgl2', name: 'tgl2'},
      {data: 'tgl3', name: 'tgl3'},
      {data: 'tgl4', name: 'tgl4'},
      {data: 'tgl5', name: 'tgl5'},
      {data: 'tgl6', name: 'tgl6'},
      {data: 'tgl7', name: 'tgl7'},
      {data: 'tgl8', name: 'tgl8'},
      {data: 'tgl9', name: 'tgl9'},
      {data: 'tgl10', name: 'tgl10'},
      {data: 'tgl11', name: 'tgl11'},
      {data: 'tgl12', name: 'tgl12'},
      {data: 'tgl13', name: 'tgl13'},
      {data: 'tgl14', name: 'tgl14'},
      {data: 'tgl15', name: 'tgl15'},
      {data: 'tgl16', name: 'tgl16'},
      {data: 'tgl17', name: 'tgl17'},
      {data: 'tgl18', name: 'tgl18'},
      {data: 'tgl20', name: 'tgl20'},
      {data: 'tgl21', name: 'tgl21'},
      {data: 'tgl22', name: 'tgl22'},
      {data: 'tgl23', name: 'tgl23'},
      {data: 'tgl24', name: 'tgl24'},
      {data: 'tgl25', name: 'tgl25'},
      {data: 'tgl26', name: 'tgl26'},
      {data: 'tgl27', name: 'tgl27'},
      {data: 'tgl28', name: 'tgl28'},
      {data: 'tgl29', name: 'tgl29'},
      {data: 'tgl30', name: 'tgl30'},
      {data: 'tgl31', name: 'tgl31'},
      {data: 'hadir', name: 'hadir'},
      {data: 'izin', name: 'izin'},
      {data: 'alfa', name: 'alfa'},
      {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
  });

  function addForm() {
    save_method = "add";
    $('input[name=_method]').val('POST');
    $('#modal-form').modal('show');
    $('#modal-form form')[0].reset();
    $('.modal-title').text('Tambah Karyawan');
  }

  function deleteData(id){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        cancelButtonColor: '#d33',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Yes, delete it!'
    }).then(function () {
        $.ajax({
            url : "{{ url('presensi') }}" + '/' + id,
            type : "POST",
            data : {'_method' : 'DELETE', '_token' : csrf_token},
            success : function(data) {
                table.ajax.reload();
                swal({
                    title: 'Success!',
                    text: 'Data has been deleted!',
                    type: 'success',
                    timer: '1500'
                })
            },
            error : function () {
                swal({
                    title: 'Oops...',
                    text: 'Something went wrong!',
                    type: 'error',
                    timer: '1500'
                })
            }
        });
    });
  }

  function editForm(id)
      {
        save_method = 'edit';
        $('input[name=_method]').val('PATCH');
        $('#modal-form form')[0].reset();
        $.ajax(
          {
            url: "{{ url('presensi') }}" + '/' + id + "/edit",
            type: "GET",
            dataType: "JSON",
            success: function(data) {
              $('#modal-form').modal('show');
              $('.modal-title').text('Edit Karyawan');

              $('#id').val(data.id);
              $('#name').val(data.name);
              $('#tgl1').val(data.tgl1);
              $('#tgl2').val(data.tgl2);
              $('#tgl3').val(data.tgl3);
              $('#tgl4').val(data.tgl4);
              $('#tgl5').val(data.tgl5);
              $('#tgl6').val(data.tgl6);
              $('#tgl7').val(data.tgl7);
              $('#tgl8').val(data.tgl8);
              $('#tgl9').val(data.tgl9);
              $('#tgl10').val(data.tgl10);
              $('#tgl11').val(data.tgl11);
              $('#tgl12').val(data.tgl12);
              $('#tgl13').val(data.tgl13);
              $('#tgl14').val(data.tgl14);
              $('#tgl15').val(data.tgl15);
              $('#tgl16').val(data.tgl16);
              $('#tgl17').val(data.tgl17);
              $('#tgl18').val(data.tgl18);
              $('#tgl19').val(data.tgl19);
              $('#tgl20').val(data.tgl20);
              $('#tgl21').val(data.tgl21);
              $('#tgl22').val(data.tgl22);
              $('#tgl23').val(data.tgl23);
              $('#tgl24').val(data.tgl24);
              $('#tgl25').val(data.tgl25);
              $('#tgl26').val(data.tgl26);
              $('#tgl27').val(data.tgl27);
              $('#tgl28').val(data.tgl28);
              $('#tgl29').val(data.tgl29);
              $('#tgl30').val(data.tgl30);
              $('#tgl31').val(data.tgl31);
              $('#hadir').val(data.hadir);
              $('#izin').val(data.izin);
              $('#alfa').val(data.alfa);

            },
            error : function() {
                alert("Nothing Data");
            }
          });
        }

  $(function()
      {
        $('#modal-form form').validator().on('submit', function (e)
        {
          if (!e.isDefaultPrevented())
          {
              var id = $('#id').val();
              if (save_method == 'add') url = "{{ url('presensi') }}";
              else url = "{{ url('presensi') . '/' }}" + id;

              $.ajax({
                  url : url,
                  type : "POST",
                  data : $('#modal-form form').serialize(),
                  success : function($data)
                  {
                      $('#modal-form').modal('hide');
                      table.ajax.reload();
                      swal({
                          title: 'Success!',
                          text: 'Data has been created!',
                          type: 'success',
                          timer: '1500'
                      })
                  },
                  error : function(){
                    swal({
                        title: 'Oops...',
                        text: 'Something went wrong!',
                        type: 'error',
                        timer: '1500'
                    })
                  }
              });
              return false;
            }
          });
      });

      function eximForm()
      {
        $('#modal-exim').modal('show');
        $('#modal-exim form')[0].reset();
      }



    </script>
  </body>
</html>
