@extends('layouts.master')
@section('content')

<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header">
      <i class="fa fa-user"></i>
      Karyawan
    </h3>
    <ol class="breadcrumb">
      <li>
        <i class="fa fa-home">
          <a href="{{route('dashboard')}}" style="color: #797979">Dashboard</a>
         </i>
      </li>

      <li>
        <i class="fa fa-user">
          <a href="{{route('nama')}}" style="color: #797979">Nama Karyawan</a>
         </i>
      </li>

      <li>
        <i class="fa fa-file-text">
          <a href="{{route('karyawan')}}" style="color: #797979">Data Admin</a>
         </i>
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
        <header class="panel-heading">Nama Karyawan</header>

        <div class="panel-body">
          <div class="col-md-12">
            <table id="presensi-table" class="table table-striped">
              <thead>
                <div class="row">
                  <div class="col-md-12">
                  <tr>
                    <th width="9">No</th>
                    <th>Nama</th>
                    <th>Hadir</th>
                    <th>Izin</th>
                    <th>Alfa</th>
                    <th>Awal Masuk</th>
                    <th>Sampai Dengan</th>
                </tr>
              </div>
              </div>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
    </div>
  </div>
</div>

<script src="{{ asset('assets/jquery/jquery-1.12.4.min.js') }}"></script>
<script src="{{ asset('assets/dataTables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/dataTables/js/dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript">
  var table = $('#presensi-table').DataTable({
              serverSide: true,
              ajax: "{{route('api.presensi')}}",
              columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'hadir', name: 'hadir'},
                {data: 'izin', name: 'izin'},
                {data: 'alfa', name: 'alfa'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'},
              ]
            });

    function addForm() {
      save_method = "add";
      $('input[name=_method]').val('POST');
      $('#modal-form').modal('show');
      $('#modal-form form')[0].reset();
      $('.modal-title').text('Tambah Karyawan');
    }

    function editForm(id)
      {
        save_method = 'edit';
        $('input[name=_method]').val('PATCH');
        $('#modal-form form')[0].reset();
        $.ajax(
          {
            url: "{{ url('presensi') }}" + '/' + id + "/edit",
            type: "GET",
            dataType: "JSON",
            success: function(data) {
              $('#modal-form').modal('show');
              $('.modal-title').text('Edit Karyawan');

              $('#id').val(data.id);
              $('#name').val(data.name);
              $('#tgl1').val(data.tgl1);
              $('#tgl2').val(data.tgl2);
              $('#tgl3').val(data.tgl3);
              $('#tgl4').val(data.tgl4);
              $('#tgl5').val(data.tgl5);
              $('#tgl6').val(data.tgl6);
              $('#tgl7').val(data.tgl7);
              $('#tgl8').val(data.tgl8);
              $('#tgl9').val(data.tgl9);
              $('#tgl10').val(data.tgl10);
              $('#tgl11').val(data.tgl11);
              $('#tgl12').val(data.tgl12);
              $('#tgl13').val(data.tgl13);
              $('#tgl14').val(data.tgl14);
              $('#tgl15').val(data.tgl15);
              $('#tgl16').val(data.tgl16);
              $('#tgl17').val(data.tgl17);
              $('#tgl18').val(data.tgl18);
              $('#tgl19').val(data.tgl19);
              $('#tgl20').val(data.tgl20);
              $('#tgl21').val(data.tgl21);
              $('#tgl22').val(data.tgl22);
              $('#tgl23').val(data.tgl23);
              $('#tgl24').val(data.tgl24);
              $('#tgl25').val(data.tgl25);
              $('#tgl26').val(data.tgl26);
              $('#tgl27').val(data.tgl27);
              $('#tgl28').val(data.tgl28);
              $('#tgl29').val(data.tgl29);
              $('#tgl30').val(data.tgl30);
              $('#tgl31').val(data.tgl31);
              $('#hadir').val(data.hadir);
              $('#izin').val(data.izin);
              $('#alfa').val(data.alfa);

            },
            error : function() {
                alert("Nothing Data");
            }
          });
        }

      function deleteData(id){
      var csrf_token = $('meta[name="csrf-token"]').attr('content');
      swal({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Yes, delete it!'
      }).then(function () {
          $.ajax({
              url : "{{ url('presensi') }}" + '/' + id,
              type : "POST",
              data : {'_method' : 'DELETE', '_token' : csrf_token},
              success : function(data) {
                  table.ajax.reload();
                  swal({
                      title: 'Success!',
                      text: 'Data has been deleted!',
                      type: 'success',
                      timer: '1500'
                  })
              },
              error : function () {
                  swal({
                      title: 'Oops...',
                      text: 'Something went wrong!',
                      type: 'error',
                      timer: '1500'
                  })
              }
          });
      });
    }

    $(function()
      {
        $('#modal-form form').validator().on('submit', function (e)
        {
          if (!e.isDefaultPrevented())
          {
              var id = $('#id').val();
              if (save_method == 'add') url = "{{ url('presensi') }}";
              else url = "{{ url('presensi') . '/' }}" + id;

              $.ajax({
                  url : url,
                  type : "POST",
                  data : $('#modal-form form').serialize(),
                  success : function($data)
                  {
                      $('#modal-form').modal('hide');
                      table.ajax.reload();
                      swal({
                          title: 'Success!',
                          text: 'Data has been created!',
                          type: 'success',
                          timer: '1500'
                      })
                  },
                  error : function(){
                    swal({
                        title: 'Oops...',
                        text: 'Something went wrong!',
                        type: 'error',
                        timer: '1500'
                    })
                  }
              });
              return false;
            }
          });
      });

</script>

@endsection
