<aside>
  <div id="sidebar" class="nav-collapse ">
    <ul class="sidebar-menu">
      <li>
        <a class="" href="{{route('dashboard')}}">
                      <i class="icon_house_alt"></i>
                      <span>Dashboard</span>
                  </a>
      </li>
      <li class="sub-menu">
        <a href="javascript:;" class="">
                      <i class="fa fa-user"></i>
                      <span>Karyawan</span>
                      <span class="menu-arrow arrow_carrot-right"></span>
                  </a>
        <ul class="sub">
          <li><a class="" href="{{route('nama')}}">Nama Karyawan</a></li>
          <li><a class="" href="{{route('karyawan')}}">Data Admin</a></li>
        </ul>
      </li>

      <li class="sub-menu">
        <a href="javascript:;" class="">
                      <i class="fa fa-bullhorn"></i>
                      <span>Laporan</span>
                      <span class="menu-arrow arrow_carrot-right"></span>
                  </a>
        <ul class="sub">
          <li><a href="{{route('cuti')}}">Cuti</a></li>
          <li><a href="{{route('cutiadmin')}}">Cuti Admin</a></li>
        </ul>
      </li>
    </ul>
  </div>
</aside>
