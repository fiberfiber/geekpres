@extends('layouts.master')
@section('content')
@include('layouts.laporan.popup.tahun')
@include('layouts.laporan.popup.namacuti')
@include('layouts.laporan.popup.developer')
@include('layouts.laporan.popup.alasan')
@include('layouts.laporan.popup.kerja')
@include('layouts.laporan.popup.time')
@include('layouts.laporan.popup.jumlah')
@include('layouts.laporan.popup.status')

<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header">
      <i class="fa fa-bullhorn"></i>
      CUTI ADMIN
    </h3>
    <ol class="breadcrumb">
      <li>
        <i class="fa fa-home">
          <a href="{{route('dashboard')}}" style="color: #797979">Dashboard</a>
         </i>
      </li>

      <li>
        <i class="fa fa-list">
          <a href="{{route('cuti')}}" style="color: #797979">Form Cuti</a>
         </i>
      </li>

      <li>
        <i class="fa fa-file-text">
          <a href="{{route('cuti')}}" style="color: #797979">Data Cuti Karyawan</a>
         </i>
      </li>

      <li>
        <i class="fa fa-user">
          <a href="{{route('cutiadmin')}}" style="color: #797979">Data Cuti Admin</a>
         </i>
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <header class="panel-heading">Form Cuti</header>
      <form action="{{route('createData')}}" class="form-horizontal" id="frm-create-class" method="POST">
      <input type="hidden" name="active" id="active" value="1">
      <input type="hidden" name="datacuti_id" id="datacuti_id">
      <div class="panel-body" style="border-bottom: 1px solid #ccc;">
          <div class="form-group">

            {{-----------------------}}
            <div class="col-sm-3 hidden">
              <label for="tahun">Tahun</label>
              <div class="input-group">
                <select class="form-control" name="tahun_id" id="tahun_id">
                  @foreach($tahuns as $key => $t)
                    <option value="{{$t->tahun_id}}">
                      {{$t->tahun}}
                    </option>
                  @endforeach
                </select>
                <div class="input-group-addon">
                  <span class="fa fa-plus" id="add-more-tahun"></span>
                </div>
              </div>
            </div>
            {{-----------------------}}
            <div class="col-sm-4 hidden">
              <label for="nama">Nama</label>
              <div class="input-group">
                <select class="form-control" name="name_id" id="name_id">
                  <option>----------------------</option>
                  @foreach($names as $key => $n)
                      <option value="{{$n->name_id}}">
                        {{$n->name}}
                      </option>
                  @endforeach
                </select>
                <div class="input-group-addon">
                  <span class="fa fa-plus" id="add-more-name"></span>
                </div>
              </div>
            </div>
            {{-----------------------}}
            <div class="col-sm-5 hidden">
              <label for="developer">Developer</label>
              <div class="input-group">
                <select class="form-control" name="developer_id" id="developer_id"></select>
                <div class="input-group-addon">
                  <span class="fa fa-plus" id="add-more-developer"></span>
                </div>
              </div>
            </div>
            {{-----------------------}}
            <div class="col-sm-3 hidden">
              <label for="project">Project</label>
              <div class="input-group">
                <select class="form-control" name="kerja_id" id="kerja_id">
                @foreach($kerjas as $key => $k)
                    <option value="{{$k->kerja_id}}">
                      {{$k->kerja}}
                    </option>
                @endforeach
                </select>
                <div class="input-group-addon">
                  <span class="fa fa-plus" id="add-more-kerja"></span>
                </div>
              </div>
            </div>
            {{-----------------------}}
            <div class="col-sm-4 hidden">
              <label for="alasan">Alasan</label>
              <div class="input-group">
                <select class="form-control" name="alasan_id" id="alasan_id">
                @foreach($alasans as $key => $a)
                    <option value="{{$a->alasan_id}}">
                      {{$a->alasan}}
                    </option>
                @endforeach
                </select>
                <div class="input-group-addon">
                  <span class="fa fa-plus" id="add-more-alasan"></span>
                </div>
              </div>
            </div>
            {{-----------------------}}
            <div class="col-sm-3 hidden">
              <label for="time">Jam Kerja</label>
              <div class="input-group">
                <select class="form-control" name="time_id" id="time_id">
                @foreach($times as $key => $t)
                    <option value="{{$t->time_id}}">
                      {{$t->time}}
                    </option>
                @endforeach
                </select>
                <div class="input-group-addon">
                  <span class="fa fa-plus" id="add-more-time"></span>
                </div>
              </div>
            </div>
            {{-----------------------}}
            <div class="col-sm-2 hidden">
              <label for="jumlah">Jumlah Hari Cuti</label>
              <div class="input-group">
                <select class="form-control" name="jumlah_id" id="jumlah_id">
                @foreach($jumlahs as $key => $j)
                    <option value="{{$j->jumlah_id}}">
                      {{$j->jumlah}}
                    </option>
                @endforeach
                </select>
                <div class="input-group-addon">
                  <span class="fa fa-plus" id="add-more-jumlah"></span>
                </div>
              </div>
            </div>
            {{-----------------------}}
            <div class="col-sm-3 hidden">
              <label for="startDate">Mulai Tanggal</label>
              <div class="input-group">
                <input type="text" name="start_date" id="start_date" class="form-control" required>
                <div class="input-group-addon">
                  <span class="fa fa-calendar"></span>
                </div>
              </div>
            </div>
            {{-----------------------}}
            <div class="col-sm-4 hidden">
              <label for="endDate">Sampai Tanggal</label>
              <div class="input-group">
                <input type="text" name="end_date" id="end_date" class="form-control" required>
                <div class="input-group-addon">
                  <span class="fa fa-calendar"></span>
                </div>
              </div>
            </div>
            {{-----------------------}}
            <div class="col-sm-2">
              <label for="status">Status</label>
              <div class="input-group">
                <select class="form-control" name="status_id" id="status_id">
                @foreach($statuss as $key => $s)
                    <option value="{{$s->status_id}}">
                      {{$s->status}}
                    </option>
                @endforeach
                </select>
                <div class="input-group-addon">
                  <span class="fa fa-plus" id="add-more-status"></span>
                </div>
              </div>
            </div>
            {{-----------------------}}

          </div>
        </div>

        <div class="panel-footer">
          <button type="submit" class="btn btn-default btn-sm">Buat Data</button>
          <button type="button" class="btn btn-success btn-sm btn-update-data">Perbarui Data</button>
        </div>
      </form>

    </div>
    <div class="panel panel-default">
      <div class="panel-heading">Data Cuti Karyawan</div>
        <div class="panel-body" id="add-data-info"></div>
    </div>
  </div>
</div>
@endsection

@section('script')
  <script type="text/javascript">
    showDataInfo($('#tahun_id').val());

    $('#start_date').datepicker(
    {
      changeMonth:true,
      changeYear:true,
      dateFormat:'yy-mm-dd'
    })

    $('#end_date').datepicker(
    {
      changeMonth:true,
      changeYear:true,
      dateFormat:'yy-mm-dd'
    })
//================================================
    $('#add-more-tahun').on('click',function()
    {
        $('#tahun-show').modal();
    })

    $('.btn-save-tahun').on('click',function()
    {
        var tahun = $('#new-tahun').val();

        $.post("{{ route('postInsertTahun') }}",{ tahun:tahun }, function(data)
        {
            $('#tahun_id').append($("<option/>",{
                value : data.tahun_id,
                text  : data.tahun
            }));

            $('new-tahun').val("");
        })
    })
//================================================
    $('#add-more-name').on('click',function()
    {
        $('#name-cuti-show').modal();
    })

    $('.btn-save-name').on('click',function()
    {
        var name = $('#new-name').val();

        $.post("{{ route('postInsertName') }}",{ name:name }, function(data)
        {
            $('#name_id').append($("<option/>",{
                value : data.name_id,
                text  : data.name
            }));

            $('new-name').val("");
        })
    })
//================================================
    $('#add-more-developer').on('click',function()
    {
        var names = $('#name_id option');
        var name = $('#frm-developer-create').find('#name_id');
        $(name).empty();
        $.each(names,function(i,pro)
        {
          $(name).append($("<option/>",
          {
                value : $(pro).val(),
                text  : $(pro).text(),
          }))
        })
        $('#developer-show').modal('show');
    })

    $('#frm-developer-create').on('submit',function(e)
    {
      e.preventDefault();
      var data = $(this).serialize();
      var url = $(this).attr('action');
      $.post(url,data,function(data)
      {
        $('#developer_id').append($("<option/>",
        {
              value : data.developer_id,
              text  : data.developer
        }))
      })
      $(this).trigger('reset');
    })

    $("#frm-create-class #name_id").on('change',function(e)
    {
      var name_id = $(this).val();
      var developer = $('#developer_id')
      $(developer).empty();
      $.get("{{ route('showDeveloper')}}",{name_id:name_id},function(data){
        $.each(data,function(i,l){
          $(developer).append($("<option/>",{
                value : l.developer_id,
                text  : l.developer
          }))
        })
      })
    })
//================================================
    $('#add-more-alasan').on('click',function()
    {
        $('#alasan-show').modal('show')
    })

    $('#frm-alasan-create').on('submit',function(e)
    {
      e.preventDefault();
      var data = $(this).serialize();
      var alasan = $('#alasan_id');
      $.post("{{ route('createAlasan') }}",data,function(data)
      {
        $(alasan).append($("<option/>",
        {
              value : data.alasan_id,
              text  : data.alasan
        }))
      })
      $(this).trigger('reset');
    })

//================================================
    $('#add-more-kerja').on('click',function()
    {
        $('#kerja-show').modal('show')
    })

    $('#frm-kerja-create').on('submit',function(e)
    {
      e.preventDefault();
      var data = $(this).serialize();
      var kerja = $('#kerja_id');
      $.post("{{ route('createKerja') }}",data,function(data)
      {
        $(kerja).append($("<option/>",
        {
              value : data.kerja_id,
              text  : data.kerja
        }))
      })
      $(this).trigger('reset');
    })

//================================================
    $('#add-more-time').on('click',function()
    {
        $('#time-show').modal('show')
    })

    $('#frm-time-create').on('submit',function(e)
    {
      e.preventDefault();
      var data = $(this).serialize();
      var time = $('#time_id');
      $.post("{{ route('createTime') }}",data,function(data)
      {
        $(time).append($("<option/>",
        {
              value : data.time_id,
              text  : data.time
        }))
      })
      $(this).trigger('reset');
    })

//================================================
    $('#add-more-jumlah').on('click',function()
    {
        $('#jumlah-show').modal('show')
    })

    $('#frm-jumlah-create').on('submit',function(e)
    {
      e.preventDefault();
      var data = $(this).serialize();
      var jumlah = $('#jumlah_id');
      $.post("{{ route('createJumlah') }}",data,function(data)
      {
        $(jumlah).append($("<option/>",
        {
              value : data.jumlah_id,
              text  : data.jumlah
        }))
      })
      $(this).trigger('reset');
    })
//================================================
    $('#add-more-status').on('click',function()
    {
        $('#status-show').modal('show')
    })

    $('#frm-status-create').on('submit',function(e)
    {
      e.preventDefault();
      var data = $(this).serialize();
      var status = $('#status_id');
      $.post("{{ route('createStatus') }}",data,function(data)
      {
        $(status).append($("<option/>",
        {
              value : data.status_id,
              text  : data.status
        }))
      })
      $(this).trigger('reset');
    })
//================================================
    $('#frm-create-class').on('submit',function(e)
    {
      e.preventDefault();
      var data = $(this).serialize();
      var url = $(this).attr('action');
      $.post(url,data,function(data)
      {
        showDataInfo(data.tahun_id);
      })
      $(this).trigger('reset');
    })

    function showDataInfo(tahun_id){
      $.get("{{route('showDataCuti')}}",{tahun_id:tahun_id}, function(data){
        $('#add-data-info').empty().append(data);
        MergeCommonRows($('#table-data-info'));
      })
    }
//================================================
    function MergeCommonRows(table)
    {
      var firstColumnBrakes = [];
      $.each(table.find('th'),function(i)
      {
      var previous = null, cellToExtend = null, rowspan =1;
        table.find("td:nth-child(" + i + ")").each(function(index,e)
        {
          var jthis = $(this), content = jthis.text();

          if (previous == content && content !== "" && $.inArray(index, firstColumnBrakes) === -1)
          {
            jthis.addClass('hidden');
            cellToExtend.attr("rowspan",(rowspan = rowspan+1));
          }
          else
          {
            if (i === 1 ) firstColumnBrakes.push(index);
            rowspan = 1;
            previous = content;
            cellToExtend = jthis;
          }
        });
      });
      $('td.hidden').remove();
    }
//================================================
  $(document).on('click','.delete-data',function(e)
  {
    datacuti_id = $(this).val();
    $.post("{{route('deleteData')}}",{datacuti_id:datacuti_id},function(data)
    {
      showDataInfo($('#tahun_id').val());
    })
  })
//================================================
  $(document).on('click','#data-edit',function(data)
  {
    var datacuti_id = $(this).data('id');
    $.get("{{route('editData')}}",{datacuti_id:datacuti_id},function(data){
      $('#tahun_id').val(data.tahun_id);
      $('#developer_id').val(data.developer_id);
      $('#alasan_id').val(data.alasan_id);
      $('#time_id').val(data.time_id);
      $('#kerja_id').val(data.kerja_id);
      $('#jumlah_id').val(data.jumlah_id);
      $('#start_date').val(data.start_date);
      $('#end_date').val(data.end_date);
      $('#datacuti_id').val(data.datacuti_id);
    })
  })
//================================================
  $('.btn-update-data').on('click',function(e)
  {
    e.preventDefault();
    var data = $('#frm-create-class').serialize();
    $.post("{{route('updateData')}}",data,function(data)
    {
      showDataInfo(data.tahun_id);
    })
  })

  </script>
@endsection
