<div class="modal fade" id="name-cuti-show" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

{{------------------------------------------------------------------------------------------------------}}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

        <h4 class="modal-title">
          Nama
        </h4>

      </div>
{{------------------------------------------------------------------------------------------------------}}
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <input type="text" name="nama_cuti" id="new-name" class="form-control"placeholder="Nama">
          </div>
        </div>
      </div>
{{------------------------------------------------------------------------------------------------------}}
      <div class="modal-footer">

        <button type="button" data-dismiss="modal" class="btn btn-default">
          Close
        </button>

        <button type="button" class="btn btn-success btn-save-name">
          Save
        </button>

      </div>
{{------------------------------------------------------------------------------------------------------}}
    </div>
  </div>
</div>
