@extends('layouts.master')
@section('content')
@include('layouts.laporan.popup.tahun')

<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header">
      <i class="icon_house_alt"></i>
      Dashboard
    </h3>
    <ol class="breadcrumb">
      <li>
        <i class="fa fa-home">
          <a href="{{route('dashboard')}}" style="color: #797979">Dashboard</a>
         </i>
      </li>

      <li>
        <i class="fa fa-bar-chart-o"></i>Grafik Kehadiran
      </li>

      <li>
        <i class="fa fa-file-text-o"></i>Data Kehadiran
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-md-3">
    <div class="panel panel-default">
      <header class="panel-heading text-center">Hadir Terbanyak</header>
      <div class="panel-body blue-bg">
        <div class="text-center">
          Ade Indra Putra
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="panel panel-default">
      <header class="panel-heading text-center">Izin Terbanyak</header>
      <div class="panel-body brown-bg">
        <div class="text-center">
          Nassharih Abdulloh
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="panel panel-default">
      <header class="panel-heading text-center">Alfa Terbanyak</header>
      <div class="panel-body dark-bg">
        <div class="text-center">
          Irfan Zainul Abidin
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="panel panel-default">
      <header class="panel-heading text-center">Hadiah Bulan Ini</header>
      <div class="panel-body green-bg">
        <div class="text-center">
          IPhone-X
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <header class="panel-heading">
          Grafik Jumlah Hadir
      </header>
      <div class="panel-body">
        <div class="col-md-12">
          {!! Charts::styles() !!}
            <div class="app">
                <center>
                    {!! $chart->html() !!}
                </center>
            </div>
            {!! Charts::scripts() !!}
            {!! $chart->script() !!}
        </div>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
        <header class="panel-heading">Data Kehadiran</header>
        <div class="panel-body">
          <div class="col-md-12">
            <table id="presensi-table" class="table table-striped">
              <thead>
                <div class="row">
                  <div class="col-md-12">
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>5</th>
                    <th>6</th>
                    <th>7</th>
                    <th>8</th>
                    <th>9</th>
                    <th>10</th>
                    <th>11</th>
                    <th>12</th>
                    <th>13</th>
                    <th>14</th>
                    <th>15</th>
                    <th>16</th>
                    <th>17</th>
                    <th>18</th>
                    <th>20</th>
                    <th>21</th>
                    <th>22</th>
                    <th>23</th>
                    <th>24</th>
                    <th>25</th>
                    <th>26</th>
                    <th>27</th>
                    <th>28</th>
                    <th>29</th>
                    <th>30</th>
                    <th>31</th>
                    <th class="text-center blue-bg">Hadir</th>
                    <th class="text-center brown-bg">Izin</th>
                    <th class="text-center dark-bg">Alfa</th>
                </tr>
              </div>
              </div>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
    </div>
  </div>
</div>


<script src="{{ asset('assets/jquery/jquery-1.12.4.min.js') }}"></script>
<script src="{{ asset('assets/dataTables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/dataTables/js/dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript">

  var table = $('#presensi-table').DataTable({
              serverSide: true,
              ajax: "{{route('api.presensi')}}",
              columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'tgl1', name: 'tgl1'},
                {data: 'tgl2', name: 'tgl2'},
                {data: 'tgl3', name: 'tgl3'},
                {data: 'tgl4', name: 'tgl4'},
                {data: 'tgl5', name: 'tgl5'},
                {data: 'tgl6', name: 'tgl6'},
                {data: 'tgl7', name: 'tgl7'},
                {data: 'tgl8', name: 'tgl8'},
                {data: 'tgl9', name: 'tgl9'},
                {data: 'tgl10', name: 'tgl10'},
                {data: 'tgl11', name: 'tgl11'},
                {data: 'tgl12', name: 'tgl12'},
                {data: 'tgl13', name: 'tgl13'},
                {data: 'tgl14', name: 'tgl14'},
                {data: 'tgl15', name: 'tgl15'},
                {data: 'tgl16', name: 'tgl16'},
                {data: 'tgl17', name: 'tgl17'},
                {data: 'tgl18', name: 'tgl18'},
                {data: 'tgl20', name: 'tgl20'},
                {data: 'tgl21', name: 'tgl21'},
                {data: 'tgl22', name: 'tgl22'},
                {data: 'tgl23', name: 'tgl23'},
                {data: 'tgl24', name: 'tgl24'},
                {data: 'tgl25', name: 'tgl25'},
                {data: 'tgl26', name: 'tgl26'},
                {data: 'tgl27', name: 'tgl27'},
                {data: 'tgl28', name: 'tgl28'},
                {data: 'tgl29', name: 'tgl29'},
                {data: 'tgl30', name: 'tgl30'},
                {data: 'tgl31', name: 'tgl31'},
                {data: 'hadir', name: 'hadir'},
                {data: 'izin', name: 'izin'},
                {data: 'alfa', name: 'alfa'},
              ]
            });

    function addForm() {
      save_method = "add";
      $('input[name=_method]').val('POST');
      $('#modal-form').modal('show');
      $('#modal-form form')[0].reset();
      $('.modal-title').text('Tambah Karyawan');
    }

    function editForm(id)
      {
        save_method = 'edit';
        $('input[name=_method]').val('PATCH');
        $('#modal-form form')[0].reset();
        $.ajax(
          {
            url: "{{ url('presensi') }}" + '/' + id + "/edit",
            type: "GET",
            dataType: "JSON",
            success: function(data) {
              $('#modal-form').modal('show');
              $('.modal-title').text('Edit Karyawan');

              $('#id').val(data.id);
              $('#name').val(data.name);
              $('#tgl1').val(data.tgl1);
              $('#tgl2').val(data.tgl2);
              $('#tgl3').val(data.tgl3);
              $('#tgl4').val(data.tgl4);
              $('#tgl5').val(data.tgl5);
              $('#tgl6').val(data.tgl6);
              $('#tgl7').val(data.tgl7);
              $('#tgl8').val(data.tgl8);
              $('#tgl9').val(data.tgl9);
              $('#tgl10').val(data.tgl10);
              $('#tgl11').val(data.tgl11);
              $('#tgl12').val(data.tgl12);
              $('#tgl13').val(data.tgl13);
              $('#tgl14').val(data.tgl14);
              $('#tgl15').val(data.tgl15);
              $('#tgl16').val(data.tgl16);
              $('#tgl17').val(data.tgl17);
              $('#tgl18').val(data.tgl18);
              $('#tgl19').val(data.tgl19);
              $('#tgl20').val(data.tgl20);
              $('#tgl21').val(data.tgl21);
              $('#tgl22').val(data.tgl22);
              $('#tgl23').val(data.tgl23);
              $('#tgl24').val(data.tgl24);
              $('#tgl25').val(data.tgl25);
              $('#tgl26').val(data.tgl26);
              $('#tgl27').val(data.tgl27);
              $('#tgl28').val(data.tgl28);
              $('#tgl29').val(data.tgl29);
              $('#tgl30').val(data.tgl30);
              $('#tgl31').val(data.tgl31);
              $('#hadir').val(data.hadir);
              $('#izin').val(data.izin);
              $('#alfa').val(data.alfa);

            },
            error : function() {
                alert("Nothing Data");
            }
          });
        }

      function deleteData(id){
      var csrf_token = $('meta[name="csrf-token"]').attr('content');
      swal({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Yes, delete it!'
      }).then(function () {
          $.ajax({
              url : "{{ url('presensi') }}" + '/' + id,
              type : "POST",
              data : {'_method' : 'DELETE', '_token' : csrf_token},
              success : function(data) {
                  table.ajax.reload();
                  swal({
                      title: 'Success!',
                      text: 'Data has been deleted!',
                      type: 'success',
                      timer: '1500'
                  })
              },
              error : function () {
                  swal({
                      title: 'Oops...',
                      text: 'Something went wrong!',
                      type: 'error',
                      timer: '1500'
                  })
              }
          });
      });
    }

    $(function()
      {
        $('#modal-form form').validator().on('submit', function (e)
        {
          if (!e.isDefaultPrevented())
          {
              var id = $('#id').val();
              if (save_method == 'add') url = "{{ url('presensi') }}";
              else url = "{{ url('presensi') . '/' }}" + id;

              $.ajax({
                  url : url,
                  type : "POST",
                  data : $('#modal-form form').serialize(),
                  success : function($data)
                  {
                      $('#modal-form').modal('hide');
                      table.ajax.reload();
                      swal({
                          title: 'Success!',
                          text: 'Data has been created!',
                          type: 'success',
                          timer: '1500'
                      })
                  },
                  error : function(){
                    swal({
                        title: 'Oops...',
                        text: 'Something went wrong!',
                        type: 'error',
                        timer: '1500'
                    })
                  }
              });
              return false;
            }
          });
      });

</script>

@endsection
