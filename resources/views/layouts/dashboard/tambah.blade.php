<div class="modal" id="modal-form" tabindex="1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="container">
        <div class="modal-content">
            <form method="post" class="form-horizontal" data-toggle="validator">
                {{csrf_field()}} {{method_field("")}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> &times; </span>
                    </button>
                    <h3 class="modal-title"></h3>
                </div>

                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <div class="form-group">
                        <label for="name" class="col-md-1 control-label">Nama</label>
                        <div class="col-md-11">
                            <input type="text" id="name" name="name" class="form-control" autofocus required>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                      <div class="modal-body col-md-2">

                        <div class="form-group">
                          <label for="tgl1" class="col-md-1 control-label">1</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl1" name="tgl1" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl2" class="col-md-1 control-label">2</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl2" name="tgl2" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl3" class="col-md-1 control-label">3</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl3" name="tgl3" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl4" class="col-md-1 control-label">4</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl4" name="tgl4" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl5" class="col-md-1 control-label">5</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl5" name="tgl5" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl6" class="col-md-1 control-label">6</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl6" name="tgl6" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                      </div>


                      <div class="modal-body col-md-2">

                        <div class="form-group">
                          <label for="tgl7" class="col-md-1 control-label">7</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl7" name="tgl7" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl8" class="col-md-1 control-label">8</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl8" name="tgl8" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl9" class="col-md-1 control-label">9</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl9" name="tgl9" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl10" class="col-md-1 control-label">10</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl10" name="tgl10" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl11" class="col-md-1 control-label">11</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl11" name="tgl11" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl12" class="col-md-1 control-label">12</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl12" name="tgl12" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                      </div>

                      <div class="modal-body col-md-2">

                        <div class="form-group">
                          <label for="tgl13" class="col-md-1 control-label">13</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl13" name="tgl13" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl14" class="col-md-1 control-label">14</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl14" name="tgl14" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl15" class="col-md-1 control-label">15</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl15" name="tgl15" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl16" class="col-md-1 control-label">16</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl16" name="tgl16" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl17" class="col-md-1 control-label">17</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl17" name="tgl17" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl18" class="col-md-1 control-label">18</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl18" name="tgl18" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                      </div>


                      <div class="modal-body col-md-2">

                        <div class="form-group">
                          <label for="tgl19" class="col-md-1 control-label">19</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl19" name="tgl19" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl20" class="col-md-1 control-label">20</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl20" name="tgl20" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl21" class="col-md-1 control-label">21</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl21" name="tgl21" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl22" class="col-md-1 control-label">22</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl22" name="tgl22" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl23" class="col-md-1 control-label">23</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl23" name="tgl23" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl24" class="col-md-1 control-label">24</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl24" name="tgl24" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>


                      </div>


                      <div class="modal-body col-md-2">

                        <div class="form-group">
                          <label for="tgl25" class="col-md-1 control-label">25</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl25" name="tgl25" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl26" class="col-md-1 control-label">26</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl26" name="tgl26" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl27" class="col-md-1 control-label">27</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl27" name="tgl27" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl28" class="col-md-1 control-label">28</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl28" name="tgl28" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl29" class="col-md-1 control-label">29</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl29" name="tgl29" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="tgl30" class="col-md-1 control-label">30</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl30" name="tgl30" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                      </div>

                      <div class="modal-body col-md-2">

                        <div class="form-group">
                          <label for="tgl31" class="col-md-1 control-label">31</label>
                          <div class="col-md-6">
                              <input type="text" id="tgl31" name="tgl31" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="hadir" class="col-md-1 control-label">Hadir</label>
                          <div class="col-md-6">
                              <input type="text" id="hadir" name="hadir" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="izin" class="col-md-1 control-label">Izin</label>
                          <div class="col-md-6">
                              <input type="text" id="izin" name="izin" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="alfa" class="col-md-1 control-label">Alfa</label>
                          <div class="col-md-6">
                              <input type="text" id="alfa" name="alfa" class="form-control">
                              <span class="help-block with-errors"></span>
                          </div>
                        </div>
                      </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="modal-exim" tabindex="1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="container">
        <div class="modal-content">
            <form method="post" action="{{route('import')}}" class="form-horizontal" data-toggle="validator" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> &times; </span>
                    </button>
                    <h3>Export/Import Data</h3>
                </div>

                <div class="modal-body">

                  <div class="form-group">
                      <label for="export" class="col-md-3 control-label">Export</label>
                      <div class="col-md-6">
                          <a href="{{route('export')}}" class="btn btn-success">Export</a>
                          <span class="help-block with-errors"></span>
                      </div>
                  </div>

                  <div class="form-group">
                      <label for="file" class="col-md-3 control-label">Import</label>
                      <div class="col-md-6">
                          <input type="file" id="file" name="file" class="form-control">
                          <span class="help-block with-errors"></span>
                      </div>
                  </div>


                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
