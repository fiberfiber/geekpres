<header class="header dark-bg">
  <div class="toggle-nav">
    <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
  </div>
  <a href="{{route('dashboard')}}" class="logo">Geek <span class="lite">Pres</span></a>

  <div class="top-nav notification-row">
    <ul class="nav pull-right top-menu">
      <li class="dropdown">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="username">{{ Auth::user()->name}}</span>
                        <b class="caret"></b>
                    </a>
        <ul class="dropdown-menu extended">
          <div class="log-arrow-up"></div>
          <li>
            <a href="{{route('logout')}}"><i class="fa fa-sign-out"></i> Log Out</a>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</header>
