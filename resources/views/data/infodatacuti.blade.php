<style type="text/css">
  .tahun-detail
  {
    white-space: normal;
    width: 400px;
  }

  #table-data-info
  {
    width: 100%;
  }

  table tbody > tr >td
  {
    vertical-align: middle;
  }

</style>

<table class="table-hover table-striped table-condensed table-bordered" id="table-data-info">
  <thead>
    <tr>
      <th>Nama</th>
      <th>Developer</th>
      <th>Project</th>
      <th>Alasan</th>
      <th>Jam Kerja</th>
      <th>Jumlah Hari Cuti</th>
      <th>Mulai Tanggal</th>
      <th>Sampai Tanggal</th>
      <th>Status</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach($datacutis as $key => $dc)
      <tr>
        <td>{{$dc->name}}</td>
        <td>{{$dc->developer}}</td>
        <td>{{$dc->kerja}}</td>
        <td>{{$dc->alasan}}</td>
        <td>{{$dc->time}}</td>
        <td>{{$dc->jumlah}}</td>
        <td>{{$dc->start_date}}</td>
        <td>{{$dc->end_date}}</td>
        <td>
            {{$dc->status}}
        </td>



        <td class="tahun-detail">
          <a href="#" data-id="{{$dc->datacuti_id}}" id="data-edit">
            <button class="btn btn-primary btn-xs" type="button" name="button"><i class="fa fa-pencil-square-o"></i> Edit</button>
            <button value="{{ $dc->datacuti_id }}" class="btn btn-danger btn-xs delete-data"><i class="fa fa-trash-o"></i> Delete</button>
          </a>
        </td>

      </tr>
    @endforeach
  </tbody>
</table>
