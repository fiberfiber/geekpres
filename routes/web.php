<?php

/*
|--------------------------------------------------------------------------
| Web Routess
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/',['as'=>'/','uses'=>'LoginController@getLogin']);
Route::post('/login',['as'=>'login','uses'=>'LoginController@postLogin']);

Route::get('/noPermission',function()
{
    return view('permission.noPermission');
});

Route::group(['middleware'=>['authen','roles']],function()
{
    Route::get('/dashboard',['as'=>'dashboard','uses'=>'DashboardController@dashboard']);
    Route::get('/logout',['as'=>'logout','uses'=>'LoginController@getLogout']);
    Route::get('/nama',['as'=>'nama','uses'=>'NamaController@getNama']);

    Route::get('/cuti',['as'=>'cuti','uses'=>'CutiController@getCuti']);

    Route::post('/cuti/tahun',['as'=>'postInsertTahun','uses'=>'CutiController@postInsertTahun']);
    Route::post('/cuti/name',['as'=>'postInsertName','uses'=>'CutiController@postInsertName']);
    Route::post('/cuti/developer',['as'=>'postInsertDeveloper','uses'=>'CutiController@postInsertDeveloper']);
    Route::get('/cuti/showDeveloper',['as'=>'showDeveloper','uses'=>'CutiController@showDeveloper']);
    Route::post('/cuti/alasan',['as'=>'createAlasan','uses'=>'CutiController@createAlasan']);
    Route::post('/cuti/kerja',['as'=>'createKerja','uses'=>'CutiController@createKerja']);
    Route::post('/cuti/time',['as'=>'createTime','uses'=>'CutiController@createTime']);
    Route::post('/cuti/jumlah',['as'=>'createJumlah','uses'=>'CutiController@createJumlah']);
    Route::post('/cuti/status',['as'=>'createStatus','uses'=>'CutiController@createStatus']);
    Route::get('/cuti/showdatacuti',['as'=>'showDataCuti','uses'=>'CutiController@showDataCuti']);
    Route::post('/cuti/datacuti',['as'=>'createData','uses'=>'CutiController@createData']);
    Route::post('/cuti/delete',['as'=>'deleteData','uses'=>'CutiController@deleteData']);
    Route::get('/cuti/edit',['as'=>'editData','uses'=>'CutiController@editData']);
    Route::post('/cuti/update',['as'=>'updateData','uses'=>'CutiController@updateData']);
});


Route::group(['middleware'=>['authen','roles'],'roles'=>['admin']],function()
{
    Route::get('/cutiadmin',['as'=>'cutiadmin','uses'=>'CutiAdminController@cutiadmin']);
    Route::get('/karyawan',['as'=>'karyawan','uses'=>'KaryawanController@karyawan']);
    Route::post('/karyawan/import',['as'=>'import','uses'=>'PresensiController@presensiImport']);
    Route::get('/karyawan/export',['as'=>'export','uses'=>'PresensiController@presensiExport']);

});


Route::resource('presensi', 'PresensiController');
Route::get('api/presensi', 'PresensiController@apiPresensi')->name('api.presensi');
