<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresensisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presensis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('tgl1')->nullable();
            $table->string('tgl2')->nullable();
            $table->string('tgl3')->nullable();
            $table->string('tgl4')->nullable();
            $table->string('tgl5')->nullable();
            $table->string('tgl6')->nullable();
            $table->string('tgl7')->nullable();
            $table->string('tgl8')->nullable();
            $table->string('tgl9')->nullable();
            $table->string('tgl10')->nullable();
            $table->string('tgl11')->nullable();
            $table->string('tgl12')->nullable();
            $table->string('tgl13')->nullable();
            $table->string('tgl14')->nullable();
            $table->string('tgl15')->nullable();
            $table->string('tgl16')->nullable();
            $table->string('tgl17')->nullable();
            $table->string('tgl18')->nullable();
            $table->string('tgl19')->nullable();
            $table->string('tgl20')->nullable();
            $table->string('tgl21')->nullable();
            $table->string('tgl22')->nullable();
            $table->string('tgl23')->nullable();
            $table->string('tgl24')->nullable();
            $table->string('tgl25')->nullable();
            $table->string('tgl26')->nullable();
            $table->string('tgl27')->nullable();
            $table->string('tgl28')->nullable();
            $table->string('tgl29')->nullable();
            $table->string('tgl30')->nullable();
            $table->string('tgl31')->nullable();
            $table->string('hadir')->nullable();
            $table->string('izin')->nullable();
            $table->string('alfa')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presensis');
    }
}
