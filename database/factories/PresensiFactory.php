<?php

use Faker\Generator as Faker;

$factory->define(App\Presensi::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'remember_token' => str_random(10),
    ];
});
