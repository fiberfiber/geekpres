<?php

use Illuminate\Database\Seeder;

class PresensisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Presensi::class, 50)->create();
    }
}
