<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
                'role_id'=>2,
                'active'=>1,
                'name'=>'Karyawan',
                'username'=>'karyawan',
                'email'=>'karyawan@geekgarden.com',
                'password'=>bcrypt('karyawan'),
                'remember_token' => str_random(10),
        ]);
    }
}
